data "ibm_resource_group" "resource_group" {
  name = "CONTENEDORES"
}

resource "ibm_container_cluster" "topaz-cluster-30" {
    name            = "topaz-cluster-30"
	datacenter      = "dal10"
    machine_type    = "b3c.4x16"	
    hardware        = "shared"
	public_vlan_id  = "3086452"
    private_vlan_id = "2965402"
	kube_version = "4.5_openshift"
    default_pool_size = 2
    public_service_endpoint  = "true"
    private_service_endpoint = "true"
	resource_group_id = data.ibm_resource_group.resource_group.id
	timeouts {
      create = "3h" 
	  }
}

resource "ibm_container_worker_pool_zone_attachment" "dal12" {
  cluster         = ibm_container_cluster.topaz-cluster-30.id
  worker_pool     = ibm_container_cluster.topaz-cluster-30.worker_pools.0.id
  zone            = "dal12"
  public_vlan_id  = "3086388"
  private_vlan_id = "3086390"
  resource_group_id = data.ibm_resource_group.resource_group.id
  wait_till_albs = false
  
  depends_on = [ ibm_container_cluster.topaz-cluster-30 ]
}

data "ibm_container_cluster_config" "cluster_config" {
  resource_group_id = data.ibm_resource_group.resource_group.id
  cluster_name_id   = ibm_container_cluster.topaz-cluster-30.id

  # Required for getting the calico configuration
  admin           = "true"
  network         = "true"
  config_dir      = "/tmp"
  
  depends_on = [ ibm_container_worker_pool_zone_attachment.dal12 ]
}

resource "null_resource" "apply-job-yaml-oracle-db" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "oracle-db.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-consultas" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-consultas.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-transacciones" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-transacciones.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-bash" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-bash.yaml"
        EOT
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}